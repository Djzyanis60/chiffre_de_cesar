import string

list_majuscule = list(string.ascii_uppercase)
list_miniscule = list(string.ascii_lowercase)



def crypte(chaine, decalage):
    resultat_cryptage = []
    for lettre in chaine:
        resultat_cryptage.append(chr(ord(lettre)+decalage))
        resultat = "".join(resultat_cryptage)
    return resultat

def decrypte(chaine, decalage):
    resultat_decryptage = []
    for lettre in chaine:
        resultat_decryptage.append(chr(ord(lettre)-decalage)) 
        resultat = "".join(resultat_decryptage)
    return resultat

def circulaire_cryptage(chaine,decalage):
    resultat_cryptage = []
    la_liste_pirate_miniscule = list_miniscule[:decalage] + list_miniscule[decalage:]
    la_liste_pirate_majuscule = list_majuscule[:decalage]+ list_majuscule[decalage:]
    for lettre in chaine:
        if lettre in la_liste_pirate_majuscule:
            resultat_cryptage.append(transcription_cryptage(lettre,decalage,la_liste_pirate_majuscule))
        elif lettre in la_liste_pirate_miniscule:
            resultat_cryptage.append(transcription_cryptage(lettre,decalage,la_liste_pirate_miniscule))
        else:
            resultat_cryptage.append(lettre)
    print(resultat_cryptage)
    resultat = "".join(resultat_cryptage)
    return resultat

def circulaire_decryptage(chaine,decalage):
    resultat_cryptage = []
    la_liste_pirate_miniscule = list_miniscule[:decalage] + list_miniscule[decalage:]
    la_liste_pirate_majuscule = list_majuscule[:decalage]+ list_majuscule[decalage:]
    for lettre in chaine:
        if lettre in la_liste_pirate_majuscule:
            resultat_cryptage.append(transcription_decryptage(lettre,decalage,la_liste_pirate_majuscule))
        elif lettre in la_liste_pirate_miniscule:
            resultat_cryptage.append(transcription_decryptage(lettre,decalage,la_liste_pirate_miniscule))
        else:
            resultat_cryptage.append(lettre)
    print(resultat_cryptage)
    resultat = "".join(resultat_cryptage)
    return resultat

def transcription_cryptage(lettre, decalage, liste):
    to = ''
    if lettre in liste:
        to = liste[(liste.index(lettre)+decalage)%len(liste)]
    else:
        to = lettre
    
    return to

def transcription_decryptage(lettre, decalage, liste):
    to = ''
    if lettre in liste:
        to = liste[(liste.index(lettre)-decalage)%len(liste)]
    else:
        to = lettre
    
    return to