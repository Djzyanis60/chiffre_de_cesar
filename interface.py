from tkinter import *
from tkinter.messagebox import *
from chiffre_de_cesar import *
import tkinter.font as tkFont
def cryptage_callback():
    showinfo('cryptage', 'cryptage : %s' % crypte(value.get(),test_nb_decalage(nb_decalage.get())))

def decryptage_callback():
   showinfo('décrypte','decrypte : %s' % decrypte(value.get(),test_nb_decalage(nb_decalage.get()))) 
def circulaire_cryptage_callback():
    showinfo('Cryptage Circulaire', 'new chaine : %s' % circulaire_cryptage(value.get(),test_nb_decalage(nb_decalage.get()))) 
def circulaire_decryptage_callback():
    showinfo('Cryptage Circulaire', 'new chaine : %s' % circulaire_decryptage(value.get(),test_nb_decalage(nb_decalage.get()))) 
def test_nb_decalage(valeur):
    if valeur.isdigit():
        return int(valeur)
    else:
            return 0



frame = Tk()
frame.title("Chiffre césar")

coms_ms = tkFont.Font(family="Comic Sans MS", size=12)
value = StringVar()
value.set("Entrer la valeur à convertir")
       
input_utilisateur = Entry(frame, textvariable=value)
input_utilisateur.grid(row=0,column=1)

button_cryptage = Button(frame, text="Cryptage", command=cryptage_callback)
button_cryptage.grid(row=1, column=2, padx= 15, pady = 15)
button_cryptage.config(height=2, bg='lightblue', fg='black', font = coms_ms)

button_decryptage = Button(frame,text="Decryptage", command=decryptage_callback)
button_decryptage.grid(row=1,column=0, padx = 15, pady = 15)
button_decryptage.config(height=2, bg='lightblue', fg='black', font=coms_ms)

button_circulaire_cryptage = Button(frame,text="Circulaire cryptage :", command=circulaire_cryptage_callback)
button_circulaire_cryptage.grid(row=0,column=0, padx = 15, pady = 15)
button_circulaire_cryptage.config(height=2, bg='lightblue', fg='black', font=coms_ms)

button_circulaire_decryptage = Button(frame,text="Circulaire decryptage :", command=circulaire_cryptage_callback)
button_circulaire_decryptage.grid(row=0,column=2, padx = 15, pady = 15)
button_circulaire_decryptage.config(height=2, bg='lightblue', fg='black', font=coms_ms)

nb_decalage= StringVar()
nb_decalage.set("nombre de décalage")

input_decalage = Entry(frame, textvariable=nb_decalage)
input_decalage.grid(row=1, column=1)

frame.mainloop()


