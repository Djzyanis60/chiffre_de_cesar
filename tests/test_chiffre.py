
import unittest
from chiffre_de_cesar import crypte,decrypte,decalage

class Test_chiffre(unittest.TestCase):

    
    def test_cryptage(self):
        int_decalage = decalage()
        if (int_decalage == 2):
            self.assertEqual(crypte("abcd"), "cdef")
        if (int_decalage == 5):
            self.assertEqual(crypte("abcd"), "fghi")
        if (int_decalage == 9):
            self.assertEqual(crypte("abcd"), "jklm")
    def test_decryptage(self):
        int_decalage = decalage()
        if (int_decalage == 13):
            self.assertEqual(decrypte("nopq"), "abcd")
        if (int_decalage == 5):
            self.assertEqual(decrypte("fghi"), "abcd")
        if (int_decalage == 3):
            self.assertEqual(decrypte("defg"), "abcd")





